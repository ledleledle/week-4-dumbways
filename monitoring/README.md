## Setup Monitoring Server

Sebelumnya buka security Group pada AWS Instance di port 9100, 3000 dan 9090.
- 9100 : Node Exporter
- 3000 : Grafana
- 9090 : Prometheus

Dan seperti pada task sebelumnya [Ansible](https://gitlab.com/ledleledle/week-4-dumbways/-/tree/master/setup-ansible), untuk installasi dan reverse proxy dari monitoring server sudah tercover oleh file ansible [Docker Container](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/docker_container.yml) dan [Install Nginx](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/install_nginx.yml).

### File docker_container.yml
Jadi container node exporter diinstall pada semua server, karena node exporter berfungsi untuk mengirimkan `metrics` yang nantinya akan diolah oleh Prometheus.
```
- hosts: all
  tasks:
  - name: Run Node Exporter
    shell: docker run -d --net="host" --pid="host" -v "/:/host:ro,rslave" --name nodeex quay.io/prometheus/node-exporter --path.rootfs=/host
```

Untuk installasi prometheus, saya menggunakan docker volume untuk menampung konfigurasi `prometheus.yml`. Untuk konfigurasinya, saya telah menyiapkannya pada folder `template` yang nantinya akan dicopy pada server monitoring.<br>
<img src="ss/1.png">

Berikut adalah perintah `ansible-playbook` yang berfungsi untuk melakukan *pull image* dan menjalankan container pada port 9090
```
  - name: Pull Prometheus
    docker_image:
     name: prom/prometheus
     pull: yes

  - name: Container Prometheus
    docker_container:
     name: prometheus
     image: prom/prometheus
     ports:
      - 9090:9090
     volumes: /home/monitoring/prometheus:/etc/prometheus
```

Sama dengan prometheus, namun untuk container grafana tidak membutuhkan docker volume.
```
  - name: Pull Grafana
    docker_image:
     name: grafana/grafana    
     pull: yes

  - name: Container Grafana
    docker_container:
     name: grafana
     image: grafana/grafana
     ports:
      - 3000:3000
```

Cek container yang berjalan pada server monitoring dengan perintah `docker ps`.
<img src="ss/2.png">

### File install_nginx.yml
Seperti yang sudah saya bahas pada task sebelumnya [(Setup Ansible)](https://gitlab.com/ledleledle/week-4-dumbways/-/tree/master/setup-ansible), untuk file reverse proxy sudah dikonfigurasikan semua jadi langkah reverse proxy pada task ini saya rasa bisa diskip. Namun hal yang bisa dilakukan untuk tahap ini yaitu membuat sub domain baru pada **Cloudflare**.
- http://monitoring2.leon.instructype.com untuk Prometheus (hanya sementara, dengan tujuan untuk mengambil metrics)
- http://monitoring.leon.instructype.com untuk Grafana

## Hasil
#### Prometheus
<img src="ss/3.png">

#### Grafana
<img src="ss/4.png">