## Connect Multiple Server to Prometheus

Sebelumnya saya sudah melakukan setup untuk server monitoring meliputi **Prometheus dan Grafana**. Sekarang yang akan saya lakukan adalah menampilkan grafik dari hasil monitoring server pada Grafana. Kenapa Grafana? Karena Prometheus tidak memberikan tampilan UI/UX yang bagus selain itu dengan Grafana saya bisa dengan mudah melakukan pengecekan pada beberapa server tanpa perlu bingung mengingat ip dari server tersebut.

Namun pertama, pastikan semua server yang akan dimonitoring sudah berstatus **UP**.
<img src="ss/1.png">

Masuk ke Grafana, untuk pertama kali Grafana akan menanyakan username & password, masukkan `admin & admin`, selanjutnya masukkan password baru (optional). Dan berikut tampilan Dashboard Grafana.
<img src="ss/2.png">

Pergi ke menu `Add your first data source > Prometheus`. Pada form URL masukkan link dari prometheus lalu klik `Save & test`.
<img src="ss/3.png">

Kembali ke home, lalu pilih menu `Create your first dashboard`. Lalu klik `Add new panel`.
<img src="ss/4.png">

Untuk data yang akan dimonitoring pada Grafana akan diambil dari Prometheus, namun harus metrics harus dicopy secara manual. Jadi buka prometheus dan cari metrics yang akan dimonitoring. Lalu copy metrics ke Grafana.
<img src="ss/5.png">

Atur tata letak dan nama metrics sesuai keinginan.
<img src="ss/6.png">