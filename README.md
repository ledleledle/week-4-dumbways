## Week 4 Bootcamp Dumbways.id

## References
#### cPanel
- https://www.phpbb.com/community/viewtopic.php?t=1023575
- https://jaluprasetyamulya.blogspot.com/2018/12/cara-unzip-file-menggunakan-ftp-client.html

#### Ansible
- https://docs.ansible.com/ansible/2.8
- https://serversforhackers.com/c/create-user-in-ansible
- https://linuxhint.com/set_mysql_root_password_ansible/
- https://www.mydailytutorials.com/ansible-add-line-to-file/

#### Monitoring
- https://prometheus.io/docs/prometheus/latest/installation/
- https://prometheus.io/docs/prometheus/latest/getting_started/
- https://www.youtube.com/watch?v=uCYfDtR9S9k