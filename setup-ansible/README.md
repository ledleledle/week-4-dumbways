## Setup Server with Ansible

Pertama, karena pada minggu ke-4 ini diharuskan untuk melakukan setup dari awal, untuk jaga-jaga, saya melakukan backup pada instance saya yang lama dan mulai setup mesin baru menggunakan ansible.
<img src="ss/1.png">

Hapus instance lama.
<img src="ss/2.png">

Buat instance baru, untuk task kali ini. Buat tambahan 2 server lagi yaitu untuk ansible dan monitoring.
<img src="ss/3.png">

Untuk setup ansible terutama pada saat instalasi paket, dibutuhkan akses SSH pada user `root`. Namun pada server AWS itu tidak diperbolehkan, jika dipaksa akan keluar output seperti berikut.<br>
<img src="ss/4.png">

Jadi ada tata cara tersendiri untuk mengakses user root. Sebelumnya, masuk dulu sebagai user biasa (ubuntu) lalu masuk ke akun root dan edit file `/root/.ssh/authorized_keys`. Hapus bagian dari file tersebut. Lakukan pada semua server yang akan di automasi menggunakan `ansible`.
```
no-port-forwarding,no-agent-forwarding,no-X11-forwarding,command="echo 'Please login as the user \"ubuntu\" rather than the user \"root\".';echo;sleep 10;exit 142" 
```

Simpan dan coba login ke user `root` dari SSH.<br>
<img src="ss/5.png">

Jika sudah dipastikan bahwa SSH dapat mengakses user root dari server-server yang akan automasi. Maka langkah selanjutnya yaitu menginstall ansible.
```
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

Jika sudah ansible sudah diinstall buat file yang berisi paket-paket dan beberapa konfigurasi yang akan dijalankan oleh `ansible-playbook`. Untuk file ansible saya dapat dicek pada repository Gitlab saya : https://gitlab.com/ledleledle/playbook-ansible. Namun saya akan mengupasnya satu per satu pada task ini.

Buat file `inventory`, file ini menyimpan semua host server yang akan diautomasi. Disini saya membuat 2 file, yang satu untuk root dan yang satu lagi untuk user biasa, karena ada proses clone repository git yang mengharuskan saya untuk menggunakan user non-root.<br>
<img src="ss/6.png">

Selanjutnya buat file `ansible.cfg`. File ini berisi konfigurasi yang akan digunakan ansible, misalnya SSH Key, letak inventory, user yang akan ditargetkan, dll.
```
[defaults]
inventory = inventory
Private_key_file = ssh/key.pem
remote_user = root
```

> **Note :** Untuk task no.1 ini (sebelumnya no.4). Saya hanya membahas tentang installasi melalui ansible untuk server frontend, backend dan jenkins saja. Karena untuk server monitoring ada task tersendiri yang harus diselesaikan (daripada task selanjutnya kosong). Karena konfigurasi ansibel beberapa sangat panjang jadi saya akan melampirkan beberapa link untuk konfigurasi ansiblenya.

#### Reverse Proxy
Pertama yaitu server reverse proxy, server ini membutuhkan nginx. Berikut file konfigurasi [Install Nginx](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/install_nginx.yml). Pertama yaitu installasi `nginx` diikut dengan penyalinan file konfigurasi untuk reverse proxy dari folder `template`, setelah dicopy string pada file tersebut akan disesuai dengan kebutuhan. Setelah string diedit, nginx akan direstart.<br>
<img src="ss/7.png">

Konfigurasi reverse proxy.<br>
<img src="ss/8.png">

#### Docker
Untuk installasi docker, saya melakukan installasi pada seluruh server, karena untuk aplikasi monitoring saya install jalankan menggunakan docker. Berikut filenya [Install Docker](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/install_docker.yml). Ansible membutuhkan jembatan antara docker dan server ansible, disini diharuskan menginstall paket `docker-py`. Dan pada setup ini, saya juga menambahkan Docker Compose.<br>
<img src="ss/9.png">

#### User Docker
Untuk penggunaan biasa, docker hanya dapat digunakan oleh user root. Pada konfigurasi [Setup User](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/add_user.yml) selain saya menambahkan group untuk docker, saya juga membuat user baru melalui ansible.<br>
<img src="ss/10.png">

#### SSH Keygen
Untuk mempermudah proses clone private repository (dumbplay). Maka saya juga membuat SSH Key, Key ini nanti juga akan ditambahkan pada Job Jenkins.
[Generate SSH Key](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/gen_ssh.yml). Namun ini hanya untuk proses generate saja, untuk penambahannya di Github/Gitlab, masih dilakukan secara manual.<br>
<img src="ss/11.png">

#### Deployment Dumbplay
Untuk Dumplay sendiri minggu kemarin sudah dibagi menjadi 2 bagian frontend dan backend, dan didalamnya terdapat file `Dockerbuild`. Jadi untuk proses ansible sendiri jadi lebih mudah. Ansible hanya perlu melakukan `git clone` dan `docker build`. Berikut file [Dumbplay](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/dumbplay.yml).

Tidak habis disana, image docker yang sudah dibuild tadi harus dijalankan pada container. Untuk konfigurasinya sebenarnya sudah tercampur dengan container lain, [Docker Container](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/docker_container.yml).
```
- hosts: frontend
  tasks:
  - name: Container frontend
    docker_container:
     name: frontend
     image: leon0408/dpfrontend
     ports: 3000:3000

- hosts: backend
  tasks:
  - name: Container backend
    docker_container:
     name: backend
     image: leon0408/dpbackend
     ports: 5000:5000
```
<img src="ss/12.png">

#### Database MySQL
Pada file [Install MySQL](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/install_mysql.yml), saya juga akan menambahkan user dan databasenya. Untuk MySQL ansible juga membutuhkan suatu paket untuk me-remote kerja MySQL, install `python3-pymysql`. Ansible akan merubah konfigurasi mysql `bind-access` menjadi `0.0.0.0`. Pada kasus ini, saya mendapatkan error bahwa mysql tidak dapat menambahkan user & database karena konfigurasi default dari MySQL, untuk mengatasinya saya menambahakn perintah untuk update plugin pada user root mysql.
```
mysql -u root -e 'UPDATE mysql.user SET plugin="mysql_native_password" WHERE user="root"'
mysql -u root -e 'FLUSH PRIVILEGES'
```

Untuk username dan password mysql, saya membuat global varialbel yang terletak pada `groups_vars/all`. Jadi ketika ingin mengganti atau menambahkan user, saya hanya perlu mengganti global variablenya. Setelah itu saya membuat konfigurasi untuk membuat user dan database baru pada mysql. Dan hasilnya seperti berikut.<br>
<img src="ss/13.png">

#### Hasil Deployment Dumbplay
<img src="ss/14.png">

#### Jenkins
Sebenarnya pada file [Install Nginx](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/install_nginx.yml) dan [Docker Container](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/docker_container.yml) sudah termasuk deployment Jenkins + Reverse Proxynya. Jadi saya tidak perlu mengkonfigurasikannya lagi.
<img src="ss/15.png">

#### Link Untuk Task Monitoring
[Link Monitoring](https://gitlab.com/ledleledle/week-4-dumbways/-/tree/master/monitoring)