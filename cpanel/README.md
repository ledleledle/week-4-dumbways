## Deployment PHP Application in cPanel

Pada task ini saya mendapatkan bagian untuk menginstall CMS phpBB. Caranya sangat mudah, disini saya menggunakan cPanel dari [byethost](https://byet.host/) cPanel ini bisa didapatkan secara gratis.

Download CMS phpBB pada website Officialnya https://www.phpbb.com/downloads dan download juga file [unzipper.php](https://github.com/ndeet/unzipper) file ini berfungsi untuk meng-ekstrak file `.zip` yang sudah diupload melalui FTP.

Karena nanti file yang sudah diekstrak akan berada didalam directory `phpBB`. Saya juga membuat file redirect sederhana yang saya letakkan pada directory utama.<br>
<img src="ss/1.png">

Untuk FTP Client, saya menggunakan FileZilla.
```
sudo pacman -Sy filezilla
```
<img src="ss/2.png">

Login pada cPanel byethost, dan dapatkan username & password untuk login FTP.
<img src="ss/3.png">

Login pada FTP Server, lalu upload file-file yang sebelumnya sudah didownload. Cara uploadnya sangat simpel, dapat di drag & drop dan tidak memerlukan perintah-perintah `bash` dan sebagainya.
<img src="ss/4.png">

Pada cPanel, buat juga database MySQL, karena phpBB akan membutuhkan MySQL untuk manajemen datanya.
<img src="ss/5.png">

Ekstrak phpBB, akses http://nama.domain.com/unzip.php
<img src="ss/6.png">

Masuk ke alamat http://nama.domain.com/phpBB lalu pilih pada tombol **Install**, installasinya sangat mudah, hanya perlu mengisi data-data yang diperlukan oleh phpBB.
<img src="ss/7.png">

Tunggu proses installasi hingga selesai.
<img src="ss/8.png">

Setelah proses installasi selesai, phpBB tidak dapat langsung digunakan, untuk dapat menggunakan CMS phpBB, folder `install` pada directory phpBB harus dihapus terlebih dahulu.
<img src="ss/9.png">

Installasi CMS phpBB pada cPanel byethost sudah selesai.
<img src="ss/10.png">